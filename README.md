# Vaje pri predmetu Numerična matematika

[![status testov](https://gitlab.com/nummat/nummat-1920/badges/master/pipeline.svg)](https://gitlab.com/nummat/nummat-1920/commits/master)
[![coverage report](https://gitlab.com/nummat/nummat-1920/badges/master/coverage.svg)](https://gitlab.com/nummat/nummat-1920/commits/master)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://nummat.gitlab.io/nummat-1920/)

# Šolsko leto 2019/20

## Sodelujoči
 * Martin Vuk (@mrcinv)

## Navodila
To je projekt z gradivi in domačimi nalogami pri predmetu [Numerična matematika](https://ucilnica.fri.uni-lj.si/1920/course/view.php?id=117).
Za praktično delo pri tem predmetu bomo uporabljali platformo GitLab, ki omogoča 
vodenje projektov in sodelovanje. 

Preberite si več o načinu dela v [vodiču za sodelovanje](CONTRIBUTING.adoc) in 
[kako poteka delo v GitLab](workflow.adoc).

# Program vaj

## Uvod

### Vaja 1
Računanje kvadratnega korena s Taylorjevo vrsto in Newtonovo metodo.

### Vaja 2
Računanje vrednosti funkcij *sin* in *cos*.

## Linerani sistemi

### Vaja 3
Implicitna interpolacija oblik z 
[radialnimi baznimi funkcijami](https://en.wikipedia.org/wiki/Radial_basis_function).

### Vaja 4
Ravnovesna lega mreže vzmeti.

## Lastne vrednosti
### Vaja 5
Invariantna porazdelitev [Markovske verige](https://en.wikipedia.org/wiki/Markov_chain)

### Vaja 6
Spektralno razvrščanje v gruče

## Interpolacija in aproksimacija
### Vaja 7
Newtonova interpolacija, Hermitova interpolacija, zlepki
### Vaja 8
Lagrangeva interpolacija v  Čebiševih točkah ([chebfun](http://www.chebfun.org/))

## Nelinearne enačbe
### Vaja 9
Konvergenčno območje Newtonove metode
## Integracija
### Vaja 10
Metoda nedoločenih koeficientov in sestavljene kvadraturne formule
### Vaja 11
Večkratni integrali
## Odvod
### Vaja 12
Numerični odvod, metoda končnih razlik za Laplaceovo enačbo (minimalne ploskve)
## Diferencialne enačbe
### Vaja 13
Eulerjeva in trapezna metoda.
### Vaja 14
Perioda geostacionarne orbite
